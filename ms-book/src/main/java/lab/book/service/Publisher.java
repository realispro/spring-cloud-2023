package lab.book.service;

import lombok.Data;

@Data
public class Publisher {

    private int id;
    private String name;
    private String logo;
}
