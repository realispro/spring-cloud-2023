package lab.book.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

//@Service
@RequiredArgsConstructor
@Slf4j
public abstract class PublisherServiceBean implements PublisherService{

    private final RestTemplate restTemplate;

    @Override
    @io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker(
            name = "publisher-service-by-id", fallbackMethod = "getPublisherFallback")
    public Publisher getPublisher(int id) {

        String publisherServiceUrl = "http://publisher-service";
        log.info("publisher service location: " + publisherServiceUrl);

        return restTemplate.exchange(
                    publisherServiceUrl + "/publishers/" + id,
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    Publisher.class
            ).getBody();
    }

    private Publisher getPublisherFallback(int id, Throwable t){
        log.error("exception while connecting with publisher-service", t);
        return null;
    }
}
