package lab.book.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class PublisherServiceFallback implements PublisherService{
    @Override
    public Publisher getPublisher(int id) {
        log.error("error while retrieving publisher {}", id);
        return null;
    }

    @Override
    public List<Publisher> getPublishers() {
        log.error("error while retrieving publishers");
        return List.of();
    }
}
