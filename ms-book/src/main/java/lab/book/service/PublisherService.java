package lab.book.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "publisher-service", fallback = PublisherServiceFallback.class)
public interface PublisherService {

    @GetMapping("/publishers/{id}")
    Publisher getPublisher(@PathVariable("id") int id);

    @GetMapping("/publishers")
    List<Publisher> getPublishers();

}
