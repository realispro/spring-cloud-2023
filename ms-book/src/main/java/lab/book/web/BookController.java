package lab.book.web;

import lab.book.data.BookRepository;
import lab.book.model.Book;
import lab.book.service.Publisher;
import lab.book.service.PublisherService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@Slf4j
public class BookController {

    private final BookRepository repository;
    private final PublisherService publisherService;

    @Value("${librarian.log.prefix}")
    private String logPrefix;

    @GetMapping("/books")
    List<Book> getBooks(){
        log.info("[" + logPrefix + "] retrieving books");
        return repository.findAll();
    }

    @GetMapping("/books/{id}")
    ResponseEntity<Book> getBookById(@PathVariable("id") int id){
        log.info("[" + logPrefix + "] retrieving book {}", id);
        Optional<Book> book = repository.findById(id);
        return ResponseEntity.of(book);
    }

    @PostMapping("/books")
    ResponseEntity<Book> addBook(@RequestBody Book book){
        log.info("[" + logPrefix + "] adding book {}", book);

        Publisher publisher = publisherService.getPublisher(book.getPublisherId());
        if(publisher==null){
            return ResponseEntity.badRequest().build();
        }

        book = repository.save(book);
        return ResponseEntity.status(HttpStatus.CREATED).body(book);
    }
}
