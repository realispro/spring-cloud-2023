package lab.publisher.cli;

import lab.publisher.data.PublisherRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
@Slf4j
public class PublisherCLI implements CommandLineRunner {

    private final PublisherRepository repository;

    @Override
    public void run(String... args) throws Exception {
        log.info("publishers: " + repository.findAll());
    }
}
