package lab.publisher.web;

import lab.publisher.data.PublisherRepository;
import lab.publisher.model.Publisher;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
public class PublisherController {

    private final PublisherRepository repository;

    @GetMapping("/publishers")
    List<Publisher> getPublishers(){
        log.info("fetching publishers");
        return repository.findAll();
    }

    @GetMapping("/publishers/{id}")
    Publisher getPublisherById(@PathVariable("id") int id){
        log.info("fetching publisher {}", id);
        return repository.findById(id).orElse(null);
    }

}
