package lab.auth.rest;

import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class UserRest {

    @GetMapping("/user")
    public Map<String, Object> getUser(OAuth2Authentication authentication){
        Map<String, Object> user = new HashMap<>();

        user.put("username", authentication.getUserAuthentication().getPrincipal());
        user.put("authorities", authentication.getUserAuthentication().getAuthorities());

        return user;
    }

}
