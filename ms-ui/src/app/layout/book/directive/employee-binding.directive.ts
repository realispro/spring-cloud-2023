import {Directive, OnDestroy, OnInit} from "@angular/core";
import {DataBindingDirective, GridComponent} from "@progress/kendo-angular-grid";
import {takeUntil} from 'rxjs/operators';
import {Subject} from "rxjs";
import {BookService} from "../../../shared/service/book.service";

@Directive({
    selector: '[employeeBinding]'
})
export class EmployeeBindingDirective extends DataBindingDirective implements OnInit, OnDestroy {

    private _$alives = new Subject();

    constructor(grid: GridComponent,
        private employeeService: BookService) {
        super(grid);
    }

    public ngOnInit(): void {
        this.employeeService.pipe(takeUntil(this._$alives)).subscribe(
            (result) => {
                this.grid.loading = false;
                this.grid.data = result;
                this.notifyDataChange();
            }
        );
        super.ngOnInit();
        this.rebind();
    }

    public ngOnDestroy(): void {
        this._$alives.next();
        this._$alives.complete();
        super.ngOnDestroy();
    }

    public rebind(): void {
        this.grid.loading = true;
        this.employeeService.query(this.state);
    }

}


