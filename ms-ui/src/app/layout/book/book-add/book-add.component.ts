import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {Router} from '@angular/router';
import {BookService} from '../../../shared/service/book.service';
import {Location} from '@angular/common';
import {PublisherService} from '../../../shared/service/publisher.service';
import {Publisher} from "../../../shared/model/publisher.model";


@Component({
  selector: 'app-employee-add',
  templateUrl: './book-add.component.html',
  styleUrls: ['./book-add.component.css']
})
export class BookAddComponent implements OnInit {

  public addBookForm: FormGroup;
  submitted = false;
  private _$alive = new Subject();
  public publishers: Publisher[];

  constructor(private _formBuilder: FormBuilder,
    private _router: Router,
    private _bookService: BookService,
    private _publisherService: PublisherService,
    private _location: Location) { }

  ngOnInit(): void {
    this.createForm();
    this._publisherService.fetchAll().subscribe(result => {
      console.log("result: %0", result)
      this.publishers = <Array<Publisher>>result;
    });
  }

  private createForm() {
    this.addBookForm = this._formBuilder.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      price: ['', Validators.required],
      cover: ['', Validators.required],
      publisherId: ['', Validators.required]
    });
  }

  get inputFieldValue() {
    return this.addBookForm.controls;
  }

  public submit() {
    this.submitted = true;
    if (this.addBookForm.invalid) {
      return;
    }
    this._bookService.save(this.addBookForm.value).subscribe(result => {
      this._router.navigate(['employee']);
    });
  }

  cancel() {
    this.addBookForm.reset();
  }

  back() {
    this._location.back();
  }
}
