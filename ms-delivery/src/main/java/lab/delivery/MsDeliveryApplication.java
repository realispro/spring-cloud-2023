package lab.delivery;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.function.Consumer;

@SpringBootApplication
@Slf4j
public class MsDeliveryApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsDeliveryApplication.class, args);
    }


    @Bean
    Consumer<String> cartConsumer(){
        return cartString -> {
            //ObjectMapper mapper = new ObjectMapper();
            //mapper.readValue(cartString, Map.class)
            log.info("order received: {}", cartString);
        };
    }

}
